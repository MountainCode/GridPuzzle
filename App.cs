using System;
using System.IO;
using Nancy;
using Nancy.Conventions;
using Nancy.Hosting.Self;

class App {
  static void Main(string[] args) {
    StaticConfiguration.DisableErrorTraces = false;
    using (var host = new NancyHost(new Uri("http://localhost:8888"))) {
      host.Start();
      Console.ReadLine();
    }
  }
}

public class CustomRootPathProvider : IRootPathProvider {
  public string GetRootPath() {
    return Path.Combine(Directory.GetCurrentDirectory(), "app");
  }
}

public class CustomBootstrapper : DefaultNancyBootstrapper {
  protected override IRootPathProvider RootPathProvider {
    get { return new CustomRootPathProvider(); }
  }
  protected override void ConfigureConventions(NancyConventions conventions) {
    base.ConfigureConventions(conventions);

    conventions.StaticContentsConventions.Add(
        StaticContentConventionBuilder.AddDirectory("js", Path.Combine("assets", "js"))
    );
  }
}
