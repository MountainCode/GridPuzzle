using System;

public class Dimensions {
  private int left;
  private int right;
  private int bottom;
  private int top;

  public Dimensions(int left, int right, int bottom, int top) {
    this.left = left;
    this.right = right;
    this.bottom = bottom;
    this.top = top;
  }

  public int Left { get { return left; } }
  public int Right { get { return right; } }
  public int Bottom { get { return bottom; } }
  public int Top { get { return top; } }

  public override string ToString() {
    var s = string.Empty;
    for(var y = 0; y < 8; ++y) {
      for(var x = 0; x < 8; ++x) {
        if(x >= Left && y >= Bottom && x <= Right && y <= Top)
          s += "■";
        else
          s += "□";
      }
      s += "\n";
    }
    return s;
  }
}
