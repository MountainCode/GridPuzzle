using System;
using System.Linq;

public class Grid : IGrid {
  int left;
  int right;
  int bottom;
  int top;

  public int TotalCalls = 0;

  public Grid(Dimensions d) {
    this.left = d.Left;
    this.right = d.Right;
    this.bottom = d.Bottom;
    this.top = d.Top;
  }

  public int CountBlocksSet(int x1, int x2, int y1, int y2) {
    ++TotalCalls;
    int intersectRight = Math.Min(x2, this.right);
    int intersectLeft = Math.Max(x1, this.left);
    int intersectTop = Math.Min(y2, this.top);
    int intersectBottom = Math.Max(y1, this.bottom);

    if (intersectRight < intersectLeft)
      return 0;
    if (intersectTop < intersectBottom)
      return 0;

    return (intersectRight - intersectLeft + 1) * (intersectTop - intersectBottom + 1);
  }
}
