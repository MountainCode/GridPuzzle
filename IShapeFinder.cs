public interface IShapeFinder {
  Shape Recognize(IGrid grid);
}
