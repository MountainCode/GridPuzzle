namespace Models {
  public class CodeBlock {
    public CodeBlock(string content) {
      this.Content = content;
    }
    public string Content { get; set; }
  }
}
