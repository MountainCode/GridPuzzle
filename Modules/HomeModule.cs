using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Models;
using Nancy;
using Nancy.Hosting.Self;
using Nustache.Core;
using dotless.Core;

public class HomeModule : NancyModule {
  public HomeModule(IRootPathProvider pathProvider) {
    Get["/"] = x => {
      var m = new Dictionary<string, Object>();
      var lessFile = Path.Combine("Style", "main.less");
      m.Add("page_title", "Demo - Home");
      m.Add("css", Less.Parse(File.ReadAllText(lessFile)));
      m.Add("snippet", File.ReadAllText("OriginalSnippet.cs"));
      return View["index", m];
    };

    Post["/"] = parameters => {
      var m = new Dictionary<string, Object>();
      var lessFile = Path.Combine("Style", "main.less");
      m.Add("page_title", "Demo - Home");
      m.Add("css", Less.Parse(File.ReadAllText(lessFile)));
      var snippet = Request.Form.code_block.ToString();
      var code = Render.FileToString(
          Path.Combine("Templates", "TestShapeFinder.cs.mustache"),
          new Dictionary<string, string> {{ "snippet", snippet }}
      );
      m.Add("code_block", new CodeBlock(code));
      m.Add("snippet", snippet);
      try {
        var shapeFinder = ShapeFinderCompiler.Compile(Nancy.Helpers.HttpUtility.HtmlDecode(code));
        var averageCalls = new ShapeFinderTester(shapeFinder).TestShapeFinder();
        m.Add("success", "You got them all right with "
            + averageCalls.ToString("0.###")
            + " function calls on average.");
      } catch (CompilerException ex) {
        m.Add("compiler_error", ex.Message);
      } catch(IncorrectShapeException ex) {
        m.Add("runtime_error", ex.Message);
        var d = ex.Dimensions;
        m.Add("dimensions",
            String.Format("Shape dimensions are left: {0}, right: {1}, top: {2}, bottom: {3}",
              d.Left, d.Right, d.Top, d.Bottom)
        );
        m.Add("box", d.ToString());
      }
      return View["index", m];
    };
  }
}
