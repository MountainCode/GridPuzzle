Grid Puzzle
===========

**WARNING THIS WEBSITE COMPILES USER SUBMITTED C# CODE. IF YOU RUN THIS ON A PUBLIC SERVER, THERE'S A GOOD CHANCE YOU WILL BE HACKED.**

Building & Running
------------------

### Windows

Make sure msbuild is in your path. If it isn't something like this should work.

```
C:\path\to\GridPuzzle> set PATH=C:\Windows\Microsoft.NET\Framework\v4.0.30319;%PATH%
```

Now just build the project and start the web server.

```
C:\path\to\GridPuzzle> msbuild.exe
C:\path\to\GridPuzzle> bin\GridPuzzle.exe
```

### Linux / Mono

With Mono on Linux, you should be able to run the following.

```bash
$ xbuild
$ mono bin/GridPuzzle.exe
```

If everything went well, you should now have a site on [localhost:8888](http://localhost:8888)
