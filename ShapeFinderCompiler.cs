using System;
using System.Collections.Generic;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Linq;

public class ShapeFinderCompiler {
  public static IShapeFinder Compile(string code) {
    var compiler = new CSharpCodeProvider(new Dictionary<string, string> {{"CompilerVersion", "v4.0"}});
    var compilerParams = new CompilerParameters {
      GenerateExecutable = false,
      GenerateInMemory = true,
      WarningLevel = 3,
      TreatWarningsAsErrors = false,
      CompilerOptions = "/optimize"
    };

    foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
      // The following assemblies will cause errors if referenced.
      string[] ignoreAssemblies = new string[] {
        "System.Reflection.Emit.AssemblyBuilder",
        "System.Reflection.Emit.InternalAssemblyBuilder"
      };
      if(Array.IndexOf(ignoreAssemblies, assembly.GetType().FullName) < 0)
        compilerParams.ReferencedAssemblies.Add(assembly.Location);
    }

    var results = compiler.CompileAssemblyFromSource(compilerParams, code);
    var errorMessage = string.Empty;
    if(results.Errors.HasErrors) {
      foreach(var line in results.Output)
        errorMessage += line + "\n";
    }
    if(errorMessage.Length > 0)
      throw new CompilerException(errorMessage);
    var t = results.CompiledAssembly.GetTypes().SingleOrDefault(lt => lt.GetInterface(typeof(IShapeFinder).Name) != null);
    return (IShapeFinder) Activator.CreateInstance(t);
  }
}
