using System;

public class ShapeFinderTester {
  const int GRID_SIZE = 8;
  private IShapeFinder shapeFinder;

  public ShapeFinderTester(IShapeFinder shapeFinder) {
    this.shapeFinder = shapeFinder;
  }

  public double TestShapeFinder() {
    int totalCalls = 0;
    int totalGridsEvaluated = 0;
    for(int left = 0; left < GRID_SIZE; ++left) {
      for(int right = left; right < GRID_SIZE; ++right) {
        for(int bottom = 0; bottom < GRID_SIZE; ++bottom) {
          for(int top = bottom; top < GRID_SIZE; ++top) {
            var dimensions = new Dimensions(left, right, bottom, top);
            var grid = new Grid(dimensions);
            Recognize(grid, dimensions);
            totalCalls += grid.TotalCalls;
            ++totalGridsEvaluated;
          }
        }
      }
    }
    return ((double)totalCalls) / ((double)totalGridsEvaluated);
  }

  private void Recognize(IGrid grid, Dimensions d) {
    var shape = shapeFinder.Recognize(grid);
    var gridIsSquare = (d.Right - d.Left == d.Top - d.Bottom);

    if(shape == Shape.Square) {
      if(!gridIsSquare)
        throw new IncorrectShapeException("Identified as Square but was Rectangle", d);
    }
    else {
      if(gridIsSquare)
        throw new IncorrectShapeException("Identified as Rectangle but was Square", d);
    }
  }
}

public class IncorrectShapeException : Exception {
  private Dimensions dimensions;
  public IncorrectShapeException(string message, Dimensions dimensions) : base(message) {
    this.dimensions = dimensions;
  }
  public Dimensions Dimensions { get { return dimensions; } }
}
