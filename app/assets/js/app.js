jQuery(function($) {
  var editor = ace.edit("editor");
  editor.setTheme("ace/theme/chaos");
  editor.getSession().setMode("ace/mode/csharp");
  var textarea = $('textarea[name="code_block"]');
  textarea.hide();
  editor.getSession().setValue(textarea.val());
  editor.getSession().on('change', function() {
    textarea.val(editor.getSession().getValue());
  });
});
